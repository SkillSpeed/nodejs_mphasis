var router = require('express').Router();
var passport = require('passport');


//auth login setup
router.get('/login', function(req, res){
	res.render('login');
})

router.get('/logout', function(req, res){
	res.send("Loggin out");
})

router.get('/google', passport.authenticate('google',{
	scope: ["profile"]
}))

router.get('/google/redirect', passport.authenticate('google'), function(req, res){
	res.send('You reached the callback');
})


module.exports = router;