const express = require('express');
const app = express();

const bodyParser = require('body-parser');

const passport = require('passport');


app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));

app.use(passport.initialize());

app.use(passport.session());

//This is for Cross Domain
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


passport.serializeUser(function(user, cb){
	cb(null, user.id);
})

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/passportdata');

const Schema = mongoose.Schema;
const UserDetail = new Schema({
	username:String,
	password: String
})

const userDetails = mongoose.model('userInfo', UserDetail, 'userInfo');

//Passport local Authentication
const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(function(username, password, done){
	console.log(username);
	userDetails.findOne({username:username}, function(err, user){
		if(err) throw err;

		if(user.password != password){
			console.log('Password not matchs');
			return done(null, false)
		}

		return done(null, user);
	})
}));

app.get('/error', function(req, res){
	res.send('Error Logging');
})

app.get('/success', function(req, res){
	res.send("User"+ req.body.username+" Logged in succesfully");
})

app.post('/user', passport.authenticate('local', {failureRequest: '/error'}), function(req, res){
	console.log('Innnn');
	res.send("Hello "+req.user.username+" Welcome to NodeJS Training");
})


app.listen(2000, function(err){
	if(err) throw err;
	console.log('Server Started with port#2000');
})