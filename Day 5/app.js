var express = require('express');
var authRoutes = require('./routes/auth-routes');
var passportSetup = require('./config/passport-setup');

var app = express();

//var ejs = require('ejs');

app.set('view engine', 'ejs');

app.use('/auth', authRoutes);

app.get('/', function(req, res){
	res.render("home");
})

app.listen(3000, () => {
	console.log('App now started with 3000 prt number!!');
})