var gulp = require('gulp');
var browserSync = require('browser-sync');


gulp.task('appServer', function(){
	
	browserSync.init({
		port:1919,
		server:{
			baseDir:['src'],
			routes:{
				'/bower_components':'bower_components'
			}
		}
	})

	gulp.watch('src/**/*.*').on('change', browserSync.reload);

})


gulp.task('testServer', function(){
	
	browserSync.init({
		port:9191,
		server:{
			baseDir:['src']
		}
	})

	gulp.watch('src/**/*.*').on('change', browserSync.reload);

})


gulp.task('default', ["appServer"])



