var fs = require('fs');
var child_process = require('child_process');

for(var i = 0; i < 2; i++){
	var wp = child_process.spawn('node', ['app.js', i]);
	
	wp.stdout.on('data', function(data){
		console.log('stdout '+data);
	})

	wp.stderr.on('data', function(data){
		console.log('stderr '+ data);
	})

	wp.on('close', function(code){
		console.log('Chil Process exit with code '+ code);
	})
}