var cluster = require('cluster');
var http = require('http');
var numCpus = require('os').cpus().length;

if(cluster.isMaster){
	console.log("Master is running");

	for(var i = 0; i < numCpus; i++){
		cluster.fork(); //creating child process
	}

	cluster.on('exit', function(worke, code, signal){
		console.log("Worker id died");
	})

}else{

	http.createServer(function(req, res){
		res.writeHead(200);
		res.end('Hello World!');
	}).listen(8000);

	console.log(`Worker ${process.pid} started`);
}