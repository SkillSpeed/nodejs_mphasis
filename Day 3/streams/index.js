var fs = require('fs');

var stream = fs.createReadStream('helloWorld.txt', 'UTF-8');
var data = "";

stream.on('data', function(chunk){
	console.log(chunk);
	console.log("-------------------------------------------------");
	data += chunk;
})

stream.on('error', function(err){
	console.log(err);
})


stream.on('end', function(){
	//console.log(data);
})

console.log('End of our program');

// fs.readFile('helloWorld.txt', 'UTF-8', function(err, data){
// 	console.log(data);
// })