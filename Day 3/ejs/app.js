var express = require('express');
var app = express();

app.set('view engine', 'ejs');

data = {};

app.get('/', function(req, res){
	res.render('home', {title: 'This is EJS template engine'})
})

app.listen(8181, function(err){
	if(err) throw err;
	console.log('Port 8181 started and up');
})