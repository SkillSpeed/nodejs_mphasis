var jade = require('jade');
var gulpJade = require('gulp-jade');
 
gulp.task('jade', function () {
  return gulp.src('./*.jade')
    .pipe(gulpJade({
      jade: jade,
      pretty: true
    }))
    .pipe(gulp.dest('./public/'))
})