var app = angular.module('myModule', ['ngRoute']);

//Creating a Route
app.config(function($routeProvider){
	$routeProvider.when('/home',{
		templateUrl:'../htmls/home.html'
	}).when('/viewall',{
		templateUrl:'../htmls/viewall.html'
	}).when('/register',{
		templateUrl:'../htmls/register.html'
	}).when('/update',{
		templateUrl:'../htmls/update.html'
	}).when('/delete',{
		templateUrl:'../htmls/delete.html'
	})
})



app.controller('myController', function($scope, $http){
	$http.get('http://localhost:2222/test').then(function(data){
		console.log(data);
		$scope.results = data.data;
	})

	$scope.addNewUser = function(){
		var payload = {'firstname':$scope.firstname, 'lastname':$scope.lastname};
		console.log(payload);
		$http({
			method:'POST',
			url:'http://localhost:2222/insertUser',
			data: payload
		}).then(function(response){
			if(response.data.status == 200){
				$(".modal").modal("hide");
				alert(response.data.msg);

			}
		}, function(response){
			console.log(response);
		})
	}

	$scope.updateUser = function(){
		var payload = {'firstname':$scope.fstn, 'lastname':$scope.lstn};
		console.log(payload);
		$http({
			method:'POST',
			url:'http://localhost:2222/updateUser',
			data: payload
		}).then(function(response){
			if(response.data.status == 200){
				$(".modal").modal("hide");
				alert(response.data.msg);

			}
		}, function(response){
			console.log(response);
		})
	}

	$scope.deleteUser = function(){
		console.log('in');
		var payload = {'firstname':$scope.del};
		console.log(payload);
		$http({
			method:'POST',
			url:'http://localhost:2222/deleteUser',
			data: payload
		}).then(function(response){
			if(response.data.status == 200){
				$(".modal").modal("hide");
				alert(response.data.msg);

			}
		}, function(response){
			console.log(response);
		})
	}

});