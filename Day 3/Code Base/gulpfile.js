var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('appServer' , function(){
	browserSync.init({
		port:9090,
		server:{
			baseDir:'build',
			routes:{
				'/bower_components':'bower_components'
			}
		}
	})

	gulp.watch(['build/**/*.*','src/**/*.*']).on('change', browserSync.reload);
})

gulp.task('testServer', function(){
	console.log('Test Server Task');
})

gulp.task('scsstocss', function(){
	console.log('scss to css');
})


gulp.task('build', ['scsstocss', 'testServer']);

gulp.task('default', ['appServer']);