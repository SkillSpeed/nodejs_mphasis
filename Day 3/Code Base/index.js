var express = require('express');
var app = express();

var bodyParser = require('body-parser');

var mongoConnect = require("./node_custom_modules/mongoconnect");


var mydb;


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//This is for Cross Domain
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

mongoConnect.mongodbConnect();


//Retrive a users from Mongo
app.use('/test', function(req, res){
	mydb.collection('personalusers').find({}).toArray(function(err, results){
		console.log(results);
		res.send(results);
	})
});

//Insert a record from Express to Mongo
app.post('/insertUser', function(req, res){
	mydb.collection('personalusers').insert(req.body, function(){
		res.send({'status':'200', 'msg':'Record Inserted'})
	});
})


app.post('/updateUser', function(req, res){	
console.log('Delete');
	var query = {'firstname':req.body.firstname};
	var updateValue = {$set : {'lastname':req.body.lastname}};
	mydb.collection('personalusers').update(query, updateValue , function(){
		console.log('1111111');
	})
})

app.post('/deleteUser', function(req, res){
	console.log('Delete'+ req.body)
	var query = {'firstname':req.body.firstname};
	mydb.collection('personalusers').remove(query, function(){
		console.log('22222222');
	})
})

app.listen(2222, function(err){
	if(err) throw err;
	console.log('Express JS Server Started !!');
})