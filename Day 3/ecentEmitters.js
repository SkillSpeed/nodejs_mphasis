var events = require('events');
var eventEmitter = new events.EventEmitter();
var util = require('util');

var Person = function(name){
	this.username = name;
}

//eventEmitter.emit('speak', "Event Triggered!!!");

util.inherits(Person, events.EventEmitter);


var jhon = new Person('Jhon')
var jezz = new Person('Jezz')
var smit = new Person('Smit')

var people = [jhon, jezz, smit];

people.forEach(function(person){
	person.on('speak', function(msg){
		console.log(msg);
	})
})

jhon.emit('speak', "Hello World! Jhon");
jezz.emit('speak', "Hello World! Jezz");
smit.emit('speak', "Hello World! Smit");

















