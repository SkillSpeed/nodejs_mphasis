var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = express();

app.use(cookieParser());
app.use(session({secret: "Don't show it to any one"}));

app.get('/', function(req, res){
	if(req.session.page_views){
		req.session.page_views++;
		res.send("Page Visits"+ req.session.page_views);
	}else{
		req.session.page_views = 1;
		res.send("Welcome to my Page");
	}
})

app.listen(2000, function(err){
	if(err) throw err;
	console.log('Server up with port 2000');
});
