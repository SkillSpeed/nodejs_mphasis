var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var session = require('express-session');

mongoose.connect('mongodb://localhost:27017/usersdata');

//DB Connections
var db = mongoose.connection;
//Schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
	firstname: {
		type:String,
		required:true
	},
	lastname: {
		type:String,
		required:true
	}
})

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json());

//This is for Cross Domain
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

db.on('error', function(err){
	if(err) throw err;
	console.log('Connection Error');
})

db.once('open', function(){
	console.log('Connected');
})

var NewUser = mongoose.model('NewUser', userSchema);
//Insert a new User
app.post('/newuser', function(req, res){
	var user = new NewUser({
		firstname:req.body.firstname,
		lastname:req.body.lastname
	});

	user.save(function(err, data){
		if(err) throw err;
		//console.log('Saved'+ data);
		//res.send("User Instered Succefully");
		res.redirect('http://localhost:9191');
	})
})

app.use(session({
	key: 'user_id',
	secret:'user must be secret',
	cookie: {
		expires : 60000
	}
}))


//Retrive all user
app.get("/getUsers", function(req, res){
	req.session.user = 'sai';
	NewUser.find({}, function(err, users){
		if(err) throw err;
		res.send(users);
	})
})

app.listen(3000, function(err){
	if(err) throw err;
	console.log('Connection Established');
})