var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('buildServer', function(){
	browserSync.init({
		port:9191,
		server:{
			baseDir:["build"],
			routes:{
				"/bower_components":"bower_components"
			}
		}
	})
	gulp.watch("/build/**/*.*").on("change", browserSync.reload);
})
