exports.mdconnection = function(){
	var mongoose = require('mongoose');
	var _url = "mongodb://localhost:27017/testdata";
	var con = mongoose.createConnection();

	//Disconnected
	var disconnected = function(err){
		if(err) throw err;
		console.log("Disconnected : "+ con.readyState);
	}

	//Connected
	var connected = function(err){
		if(err) throw err;
		console.log("Connected : "+ con.readyState);
	}

	//connecting
	var connecting = function(err){
		if(err) throw err;
		console.log("Connection : "+ con.readyState);
	}

	//dicsonnecting
	var disconnecting = function(err){
		if(err) throw err;
		console.log("Disconnecting : "+ con.readyState);
	}

	con.on("disconnected", disconnected);
	con.on("connected", connected);
	con.on("connecting", connecting);
	con.on("disconnecting", disconnecting);


	//var con = mongoose.createConnection();
	var cfn = function(err){
		if(err) throw err;
		console.log('Connection Established');
	}
	//npm install mongoose@4.10.8
	con.open("mongodb://localhost:27017", cfn);

}