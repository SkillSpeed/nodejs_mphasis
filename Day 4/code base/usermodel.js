
var userSchema = new mongoose.Schema({
	name: String,
	username: {type: String, required:true, unique: true},
	password: {type: String, required:true},
	age: Number,
})

var User = mongoose.model('User', userSchema);


module.exports = User;
