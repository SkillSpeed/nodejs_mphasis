var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/usersdata');

var db = mongoose.connection;

db.on('error', function(err){
	if(err) throw err;
	console.log('Connection Error');
})

db.once('open', function(){
	console.log('Connected');
})

var Schema = mongoose.Schema;
var userSchema = new Schema({
	name: String,
	DOB: Date
})


var User = mongoose.model('User', userSchema);

var jhon = new User({
	name:"Will Smit",
	DOB:'01/01/2018'
});

jhon.save(function(err, data){
	if(err) throw err;
	console.log('Saved'+ data);
})