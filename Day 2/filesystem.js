var express = require('express');
var app = express();

var fs = require('fs');
var bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json());

app.use('/createFile',function(req, res){
	//We retriving the file name from front end as a request headers
	var filename = req.body.createname;

	//appen the file extension
		filename = "./myfiles/"+filename+".txt";

	//File content
	var content = "Hello World from Dynamic files";

    //Create a file using file system
	fs.writeFile(filename, content, function(err){
		if(err) throw err;
		res.send('File Created');
	})
	
});

app.use('/readFile', function(req, res){
	var filename = req.body.readname;
		filename = "./myfiles/"+filename+".txt";
	fs.readFile(filename, function(err, result){
		if(err) throw err;
		//console.log('hello')
		result = ""+result;
		res.send(result);
	})
})

app.use('/deleteFile', function(req, res){
	var filename = req.body.deletename;
		filename = "./myfiles/"+ filename +".txt";
	fs.unlink(filename, function(err){
		if(err) throw err;

		res.send('File Deleted!!')
	})
})






app.listen(3000, function(err){
	if(err) throw err;
	console.log('Server got created using 3000 port number!!');
})