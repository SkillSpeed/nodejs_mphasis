var express = require('express');
var app = express();

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// app.use('/', function(request, response){
// 	console.log('Hello World From Express JS !!');
// })

//db.users.insert({'firstname':'Vinod','lastname':'Varma'});

app.use('/test', function(req, res){
	MongoClient.connect(url, function(err, db){
		if(err) throw err;
		console.log("Connection Established!!!");
		var appDB = db.db('users');
		// appDB.collection('personalusers').find({}, function(err, result){
		// 	console.log(results);
		// });
		appDB.collection('personalusers').find({}).toArray(function(err, results){
			console.log(results);
			res.send(results);
		})


		//res.send('MongoDB Connect Enstablished!!!');
	})
});

app.listen(2222, function(err){
	if(err) throw err;
	console.log('Express JS Server Started !!');
})