var fs = require('fs');

//read a file
// fs.readFile('helloWorld.txt', function(err, data){
// 	if(err) return console.log(err);
// 	console.log(" "+data +" ");
// })

//Update a file
// fs.appendFile('newFiles/helloWorld.txt', 'This is Vinod', function(err){
// 	if(err) throw err;
// 	console.log('File Created');
// })

// Create a File
// var fn_test = function (){
// 	console.log('yes done')
// }

// fs.writeFile('newFiles/test.txt', 'Hello', fn_test)


//Delete
fs.unlink('newFiles/helloWorld.txt', function(err){
	if(err) throw err;
	console.log('File got Deleted!!!');
})
