var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');

var url = "mongodb://localhost:27017/";


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json());

//This is for Cross Domain
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Retrive a user account from MongoDB
app.post('/userlogin', function(req, res){
	MongoClient.connect(url, function(err, db){
		if(err) throw err;
			var DB = db.db('testdata');
			DB.collection('userlogin').find({}).toArray(function(err, results){
				var resMsg = "User Not Found !!";
				for(var i = 0; i < results.length; i++){
					if(req.body.username === results[i].username && req.body.password === results[i].password){
						console.log('inn');
						resMsg = "User Logged In";
					}
				}
				res.send(resMsg);
		})
	})
});

//Insert a New User
app.post('/insertUser', function(req, res){
	MongoClient.connect(url,  function(err, db){
		if(err) throw err;
		var DB = db.db('testdata');
			DB.collection('userlogin').insert(req.body, function(){
				res.send('Record Inserted');
			})
	})
});

app.listen(3000, function(err){
	if(err) throw err;
	console.log("Middleware server started with 3000 Port Number!")
})